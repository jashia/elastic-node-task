var mongoose = require('mongoose');
var Talk = mongoose.model('Talk');

var sendJSONresponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

/* POST a new comment, providing a talkid */
/* /api/talks/:talkid/comments */
module.exports.commentsCreate = function(req, res) {
  if (req.params.talkid) {
    Talk
      .findById(req.params.talkid)
      .select('comments')
      .exec(
        function(err, talk) {
          if (err) {
            sendJSONresponse(res, 400, err);
          } else {
            doAddComment(req, res, talk);
          }
        }
    );
  } else {
    sendJSONresponse(res, 404, {
      "message": "Not found, talkid required"
    });
  }
};


var doAddComment = function(req, res, talk) {
  if (!talk) {
    sendJSONresponse(res, 404, "talkid not found");
  } else {
    talk.comments.push({
      author: req.body.author,
      rating: req.body.rating,
      commentText: req.body.commentText
    });
    talk.save(function(err, talk) {
      var thisComment;
      if (err) {
        sendJSONresponse(res, 400, err);
      } else {
        updateAverageRating(talk._id);
        thisComment = talk.comments[talk.comments.length - 1];
        sendJSONresponse(res, 201, thisComment);
      }
    });
  }
};

var updateAverageRating = function(talkid) {
  console.log("Update rating average for", talkid);
  Talk
    .findById(talkid)
    .select('comments')
    .exec(
      function(err, talk) {
        if (!err) {
          doSetAverageRating(talk);
        }
      });
};

var doSetAverageRating = function(talk) {
  var i, commentCount, ratingAverage, ratingTotal;
  if (talk.comments && talk.comments.length > 0) {
    commentCount = talk.comments.length;
    ratingTotal = 0;
    for (i = 0; i < commentCount; i++) {
      ratingTotal = ratingTotal + talk.comments[i].rating;
    }
    ratingAverage = parseInt(ratingTotal / commentCount, 10);
    talk.rating = ratingAverage;
    talk.save(function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Average rating updated to", ratingAverage);
      }
    });
  }
};

module.exports.commentsUpdateOne = function(req, res) {
  if (!req.params.talkid || !req.params.commentid) {
    sendJSONresponse(res, 404, {
      "message": "Not found, talkid and commentid are both required"
    });
    return;
  }
  Talk
    .findById(req.params.talkid)
    .select('comments')
    .exec(
      function(err, talk) {
        var thisComment;
        if (!talk) {
          sendJSONresponse(res, 404, {
            "message": "talkid not found"
          });
          return;
        } else if (err) {
          sendJSONresponse(res, 400, err);
          return;
        }
        if (talk.comments && talk.comments.length > 0) {
          thisComment = talk.comments.id(req.params.commentid);
          if (!thisComment) {
            sendJSONresponse(res, 404, {
              "message": "commentid not found"
            });
          } else {
            thisComment.author = req.body.author;
            thisComment.rating = req.body.rating;
            thisComment.commentText = req.body.commentText;
            talk.save(function(err, talk) {
              if (err) {
                sendJSONresponse(res, 404, err);
              } else {
                updateAverageRating(talk._id);
                sendJSONresponse(res, 200, thisComment);
              }
            });
          }
        } else {
          sendJSONresponse(res, 404, {
            "message": "No comment to update"
          });
        }
      }
  );
};

module.exports.commentsReadOne = function(req, res) {
  console.log("Getting single comment");
  if (req.params && req.params.talkid && req.params.commentid) {
    Talk
      .findById(req.params.talkid)
      .select('name comments')
      .exec(
        function(err, talk) {
          console.log(talk);
          var response, comment;
          if (!talk) {
            sendJSONresponse(res, 404, {
              "message": "talkid not found"
            });
            return;
          } else if (err) {
            sendJSONresponse(res, 400, err);
            return;
          }
          if (talk.comments && talk.comments.length > 0) {
            comment = talk.comments.id(req.params.commentid);
            if (!comment) {
              sendJSONresponse(res, 404, {
                "message": "commentid not found"
              });
            } else {
              response = {
                talk: {
                  name: talk.name,
                  id: req.params.talkid
                },
                comment: comment
              };
              sendJSONresponse(res, 200, response);
            }
          } else {
            sendJSONresponse(res, 404, {
              "message": "No comments found"
            });
          }
        }
    );
  } else {
    sendJSONresponse(res, 404, {
      "message": "Not found, talkid and commentid are both required"
    });
  }
};

// app.delete('/api/talks/:talkid/comments/:commentid'
module.exports.commentsDeleteOne = function(req, res) {
  if (!req.params.talkid || !req.params.commentid) {
    sendJSONresponse(res, 404, {
      "message": "Not found, talkid and commentid are both required"
    });
    return;
  }
  Talk
    .findById(req.params.talkid)
    .select('comments')
    .exec(
      function(err, talk) {
        if (!talk) {
          sendJSONresponse(res, 404, {
            "message": "talkid not found"
          });
          return;
        } else if (err) {
          sendJSONresponse(res, 400, err);
          return;
        }
        if (talk.comments && talk.comments.length > 0) {
          if (!talk.comments.id(req.params.commentid)) {
            sendJSONresponse(res, 404, {
              "message": "commentid not found"
            });
          } else {
            talk.comments.id(req.params.commentid).remove();
            talk.save(function(err) {
              if (err) {
                sendJSONresponse(res, 404, err);
              } else {
                updateAverageRating(talk._id);
                sendJSONresponse(res, 204, null);
              }
            });
          }
        } else {
          sendJSONresponse(res, 404, {
            "message": "No comment to delete"
          });
        }
      }
  );
};
