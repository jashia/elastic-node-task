var express = require('express');
var router = express.Router();
var ctrlTalks = require('../controllers/talks');
var ctrlComments = require('../controllers/comments');

router.get('/talks', ctrlTalks.talksListByDistance);
router.post('/talks', ctrlTalks.talksCreate);
router.get('/talks/:talkid', ctrlTalks.talksReadOne);
router.put('/talks/:talkid', ctrlTalks.talksUpdateOne);
router.delete('/talks/:talkid', ctrlTalks.talksDeleteOne);

// comments
router.post('/talks/:talkid/comments', ctrlComments.commentsCreate);
router.get('/talks/:talkid/comments/:commentid', ctrlComments.commentsReadOne);
router.put('/talks/:talkid/comments/:commentid', ctrlComments.commentsUpdateOne);
router.delete('/talks/:talkid/comments/:commentid', ctrlComments.commentsDeleteOne);

module.exports = router;
