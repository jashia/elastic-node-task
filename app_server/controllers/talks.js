var request = require('request');
var apiOptions = {
  server : "http://localhost:3000"
};
if (process.env.NODE_ENV === 'production') {
  apiOptions.server = "https://intense-brushlands-52038.herokuapp.com";
}

var _isNumeric = function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

var _formatDistance = function (distance) {
  var numDistance, unit;
  if (distance && _isNumeric(distance)) {
    if (distance > 1) {
      numDistance = parseFloat(distance).toFixed(1);
      unit = 'km';
    } else {
      numDistance = parseInt(distance * 1000,10);
      unit = 'm';
    }
    return numDistance + unit;
  } else {
    return "?";
  }
};

var _showError = function (req, res, status) {
  var title, content;
  if (status === 404) {
    title = "404, page not found";
    content = "Once upon a midnight dreary, while I pondered weak and weary, Over many a quaint and curious volume of fogotten lore...something, something...Page not found 404.";
  } else if (status === 500) {
    title = "500, internal server error";
    content = "How embarrassing. There's a problem with our server.";
  } else {
    title = status + ", something's gone wrong";
    content = "Something, somewhere, has gone just a little bit wrong.";
  }
  res.status(status);
  res.render('generic-text', {
    title : title,
    content : content
  });
};

var renderHomepage = function(req, res, responseBody){
  var message;
  if (!(responseBody instanceof Array)) {
    message = "API lookup error";
    responseBody = [];
  } else {
    if (!responseBody.length) {
      message = "No places found nearby";
    }
  }
  res.render('talks-list', {
    title: 'EventMe - find a your seminar, session, or talk',
    pageHeader: {
      title: 'Event Me',
      strapline: ' - find a your seminar, session, or talk'
    },
    sidebar: "Looking for your session? Event Me is here to help.",
    talks: responseBody,
    message: message
  });
};

/* GET 'home' page */
module.exports.homelist = function(req, res){
  var requestOptions, path;
  path = '/api/talks';
  requestOptions = {
    url : apiOptions.server + path,
    method : "GET",
    json : {},
    qs : {
      lng : -0.9690884,
      lat : 51.455041,
      maxDistance : 200000
    }
  };
  request(
    requestOptions,
    function(err, response, body) {
      var i, data;
      data = body;
      if (response.statusCode === 200 && data.length) {
        for (i=0; i<data.length; i++) {
          data[i].distance = _formatDistance(data[i].distance);
        }
      }
      renderHomepage(req, res, data);
    }
  );
};

var getTalkInfo = function (req, res, callback) {
  var requestOptions, path;
  path = "/api/talks/" + req.params.talkid;
  requestOptions = {
    url : apiOptions.server + path,
    method : "GET",
    json : {}
  };
  request(
    requestOptions,
    function(err, response, body) {
      var data = body;
      if (response.statusCode === 200) {
        data.coords = {
          lng : body.coords[0],
          lat : body.coords[1]
        };
        callback(req, res, data);
      } else {
        _showError(req, res, response.statusCode);
      }
    }
  );
};

var renderDetailPage = function (req, res, talkDetail) {
  res.render('talk-info', {
    title: talkDetail.name,
    pageHeader: {title: talkDetail.name},
    sidebar: {
      context: 'is on eventMe because it has a space to sit down with your laptop and get some work done.',
      callToAction: 'If you\'ve been and you like it - or if you don\'t - please leave a comment to help other people just like you.'
    },
    talk: talkDetail
  });
};

/* GET 'Talk info' page */
module.exports.talkInfo = function(req, res){
  getTalkInfo(req, res, function(req, res, responseData) {
    renderDetailPage(req, res, responseData);
  });
};

var renderCommentForm = function (req, res, talkDetail) {
  res.render('talk-comment-form', {
    title: 'Comment on' + talkDetail.name + ' on Event Me',
    pageHeader: { title: 'Comment ' + talkDetail.name },
    error: req.query.err
  });
};

/* GET 'Add comment' page */
module.exports.addComment = function(req, res){
  getTalkInfo(req, res, function(req, res, responseData) {
    renderCommentForm(req, res, responseData);
  });
};

/* POST 'Add comment' page */
module.exports.doAddComment = function(req, res){
  var requestOptions, path, talkid, postdata;
  talkid = req.params.talkid;
  path = "/api/talks/" + talkid + '/comments';
  postdata = {
    author: req.body.name,
    rating: parseInt(req.body.rating, 10),
    commentText: req.body.comment
  };
  requestOptions = {
    url : apiOptions.server + path,
    method : "POST",
    json : postdata
  };
  if (!postdata.author || !postdata.rating || !postdata.commentText) {
    res.redirect('/talk/' + talkid + '/comments/new?err=val');
  } else {
    request(
      requestOptions,
      function(err, response, body) {
        if (response.statusCode === 201) {
          res.redirect('/talk/' + talkid);
        } else if (response.statusCode === 400 && body.name && body.name === "ValidationError" ) {
          res.redirect('/talk/' + talkid + '/comments/new?err=val');
        } else {
          console.log(body);
          _showError(req, res, response.statusCode);
        }
      }
    );
  }
};