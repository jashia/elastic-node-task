var mongoose = require('mongoose');
var Talk = mongoose.model('Talk');

var sendJSONresponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

var theEarth = (function() {
  var earthRadius = 6371; // km, miles is 3959

  var getDistanceFromRads = function(rads) {
    return parseFloat(rads * earthRadius);
  };

  var getRadsFromDistance = function(distance) {
    return parseFloat(distance / earthRadius);
  };

  return {
    getDistanceFromRads: getDistanceFromRads,
    getRadsFromDistance: getRadsFromDistance
  };
})();

/* GET list of talks */
module.exports.talksListByDistance = function(req, res) {
  var lng = parseFloat(req.query.lng);
  var lat = parseFloat(req.query.lat);
  var maxDistance = parseFloat(req.query.maxDistance);
  var point = {
    type: "Point",
    coordinates: [lng, lat]
  };
  var geoOptions = {
    spherical: true,
    maxDistance: theEarth.getRadsFromDistance(maxDistance),
    num: 10
  };
  if ((!lng && lng!==0) || (!lat && lat!==0) || ! maxDistance) {
    console.log('talksListByDistance missing params');
    sendJSONresponse(res, 404, {
      "message": "lng, lat and maxDistance query parameters are all required"
    });
    return;
  }
  Talk.geoNear(point, geoOptions, function(err, results, stats) {
    var talks;
    console.log('Geo Results', results);
    console.log('Geo stats', stats);
    if (err) {
      console.log('geoNear error:', err);
      sendJSONresponse(res, 404, err);
    } else {
      talks = buildTalkList(req, res, results, stats);
      sendJSONresponse(res, 200, talks);
    }
  });
};

var buildTalkList = function(req, res, results, stats) {
  var talks = [];
  results.forEach(function(doc) {
    talks.push({
      distance: theEarth.getDistanceFromRads(doc.dis),
      name: doc.obj.name,
      address: doc.obj.address,
      rating: doc.obj.rating,
      facilities: doc.obj.facilities,
      _id: doc.obj._id
    });
  });
  return talks;
};

/* GET a talk by the id */
module.exports.talksReadOne = function(req, res) {
  console.log('Finding talk details', req.params);
  if (req.params && req.params.talkid) {
    Talk
      .findById(req.params.talkid)
      .exec(function(err, talk) {
        if (!talk) {
          sendJSONresponse(res, 404, {
            "message": "talkid not found"
          });
          return;
        } else if (err) {
          console.log(err);
          sendJSONresponse(res, 404, err);
          return;
        }
        console.log(talk);
        sendJSONresponse(res, 200, talk);
      });
  } else {
    console.log('No talkid specified');
    sendJSONresponse(res, 404, {
      "message": "No talkid in request"
    });
  }
};

/* POST a new talk */
/* /api/talks */
module.exports.talksCreate = function(req, res) {
  console.log(req.body);
  Talk.create({
    name: req.body.name,
    address: req.body.address,
    facilities: req.body.facilities.split(","),
    coords: [parseFloat(req.body.lng), parseFloat(req.body.lat)],
    openingTimes: [{
      days: req.body.days1,
      opening: req.body.opening1,
      closing: req.body.closing1,
      closed: req.body.closed1,
    }, {
      days: req.body.days2,
      opening: req.body.opening2,
      closing: req.body.closing2,
      closed: req.body.closed2,
    }]
  }, function(err, talk) {
    if (err) {
      console.log(err);
      sendJSONresponse(res, 400, err);
    } else {
      console.log(talk);
      sendJSONresponse(res, 201, talk);
    }
  });
};

/* PUT /api/talk/:talkid */
module.exports.talksUpdateOne = function(req, res) {
  if (!req.params.talkid) {
    sendJSONresponse(res, 404, {
      "message": "Not found, talkid is required"
    });
    return;
  }
  Talk
    .findById(req.params.talkid)
    .select('-reviews -rating')
    .exec(
      function(err, talk) {
        if (!talk) {
          sendJSONresponse(res, 404, {
            "message": "talkid not found"
          });
          return;
        } else if (err) {
          sendJSONresponse(res, 400, err);
          return;
        }
        talk.name = req.body.name;
        talk.address = req.body.address;
        talk.facilities = req.body.facilities.split(",");
        talk.coords = [parseFloat(req.body.lng), parseFloat(req.body.lat)];
        talk.openingTimes = [{
          days: req.body.days1,
          opening: req.body.opening1,
          closing: req.body.closing1,
          closed: req.body.closed1,
        }, {
          days: req.body.days2,
          opening: req.body.opening2,
          closing: req.body.closing2,
          closed: req.body.closed2,
        }];
        talk.save(function(err, talk) {
          if (err) {
            sendJSONresponse(res, 404, err);
          } else {
            sendJSONresponse(res, 200, talk);
          }
        });
      }
  );
};

/* DELETE /api/talk/:talkid */
module.exports.talksDeleteOne = function(req, res) {
  var talkid = req.params.talkid;
  if (talkid) {
    Loc
      .findByIdAndRemove(talkid)
      .exec(
        function(err, talk) {
          if (err) {
            console.log(err);
            sendJSONresponse(res, 404, err);
            return;
          }
          console.log("Talk id " + talkid + " deleted");
          sendJSONresponse(res, 204, null);
        }
    );
  } else {
    sendJSONresponse(res, 404, {
      "message": "No talkid"
    });
  }
};
