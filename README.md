# Event Me - The Nodejs and Express app to help you find great talks, sessions, and events close to you. 

This was a node challenge to create a working prototype of an event / conference app with user comment ability. 

Currently there are only 3 talks available for comments. The next version of this app will probably bring in an Angular or React Frontend to manage users creating their own talks. 

Additionally, there will be some authentication put on this in the near future. 

The app is hosted on Heroku at this address: 

https://intense-brushlands-52038.herokuapp.com/

You can get the code from this repo at any time: 

https://jashia@bitbucket.org/jashia/elastic-node-task.git

The DB is hosted on mLab. 

Enjoy!