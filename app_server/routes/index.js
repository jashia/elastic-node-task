var express = require('express');
var router = express.Router();
var ctrlTalks = require('../controllers/talks');
var ctrlInfos = require('../controllers/infos');

// Talks pages

router.get('/', ctrlTalks.homelist);
router.get('/talk/:talkid', ctrlTalks.talkInfo);
router.get('/talk/:talkid/comment/new', ctrlTalks.addComment);
router.post('/talk/:talkid/comment/new', ctrlTalks.doAddComment);

// Info Pages 

router.get('/about', ctrlInfos.about);

/* GET home page. */

//router.get('/', ctrlMain.index);

module.exports = router;
