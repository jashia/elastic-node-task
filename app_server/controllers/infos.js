// GET About page 
module.exports.about = function(req, res) {
	res.render('generic-text', {
		title: 'About eventMe',
		content: 'Event Me was created as a Node challenge and test for Ja.'
	});
};
